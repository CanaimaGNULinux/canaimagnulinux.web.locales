.. -*- coding: utf-8 -*-

.. contents:: Tabla de Contenidos

Introducción
============

Este producto contiene las `localizaciones`_ de paquetes para 
`sitio web del proyecto Canaima GNU/Linux`_. Esto permite sobreescribir 
las traducciones al Español de Venezuela de ciertos productos Plone.

Características
===============

- Contiene las localizaciones al Español Venezuela (es-VE) de los siguientes
  paquetes:

  - Producto `quintagroup.analytics`_ para el dominio ``quintagroup.analytics``.
  - Producto `collective.nitf`_ para los dominios ``collective.nitf`` y ``plone``.

Instalación
===========

Usted puede leer el archivo ``INSTALL.txt`` dentro del directorio ``docs`` de
este paquete.

Descargas
=========

Usted puede encontrar la versión de desarrollo del paquete ``canaimagnulinux.web.locales``
en el `repositorio CanaimaGNULinux`_ en GitHub.com.

Autor(es) Original(es)
======================

* Leonardo J .Caballero G. aka macagua

Colaboraciones impresionantes
=============================

* Nombre Completo aka apodo


Para una lista actualizada de todo los colaboradores visite:
https://github.com/canaimagnulinux/canaimagnulinux.web.locales/contributors

.. _`sitio web del proyecto Canaima GNU/Linux`: http://canaima.softwarelibre.gob.ve/
.. _`localizaciones`: http://es.wikipedia.org/wiki/Internacionalización_y_localización
.. _`quintagroup.analytics`: https://pypi.python.org/pypi/quintagroup.analytics
.. _`collective.nitf`: https://github.com/collective/collective.nitf
.. _`repositorio CanaimaGNULinux`: https://github.com/CanaimaGNULinux/canaimagnulinux.web.locales
