.. -*- coding: utf-8 -*-

.. image:: https://d2weczhvl823v0.cloudfront.net/CanaimaGNULinux/canaimagnulinux.web.locales/trend.png
   :alt: Bitdeli badge
   :target: https://bitdeli.com/free

.. contents:: Tabla de Contenidos

Introducción
============

Este producto contiene las `localizaciones`_ de paquetes para 
`sitio web del proyecto Canaima GNU/Linux`_. Esto permite sobreescribir 
las traducciones al Español de Venezuela de ciertos productos Plone.

Características
===============

- Contiene las localizaciones al Español Venezuela (es-VE) de los siguientes
  paquetes:

  - Producto `z3c.form`_ para el dominio ``z3c.form``.

  - Producto `plone.app.ldap`_ para el dominio ``plone.app.ldap``.

  - Producto `plone.app.caching`_ para el dominio ``plone.app.caching``.

  - Producto `plone.app.dexterity`_ para el dominio ``plone.app.dexterity``.

  - Producto `plone.schemaeditor`_ para el dominio ``plone.schemaeditor``.

  - Producto `plone.app.discussion`_ para el dominio ``plone.app.discussion``.

  - Producto `quintagroup.analytics`_ para el dominio ``quintagroup.analytics``.

  - Producto `collective.nitf`_ para los dominios ``collective.nitf`` y ``plone``.

  - Producto `collective.geo.settings`_ para el dominio ``collective.geo.settings``.

  - Producto `pas.plugins.velruse`_ para el dominio ``pas.plugins.velruse``.

  - Producto `Products.TinyMCE`_ para el dominio ``tinymce``.

  - Producto `Products.PloneServicesCenter`_ para el dominio ``ploneservicescenter``.

Instalación
===========

Usted puede leer el archivo ``INSTALL.txt`` dentro del directorio ``docs`` de
este paquete.

Descargas
=========

Usted puede encontrar la versión de desarrollo del paquete ``canaimagnulinux.web.locales``
en el `repositorio CanaimaGNULinux`_ en GitHub.com.

¿Tienes una idea?, ¿Encontraste un error? Háganos saber mediante la `apertura de un ticket de soporte`_.


Autor(es) Original(es)
======================

* Leonardo J .Caballero G. aka macagua

Colaboraciones impresionantes
=============================

* Flamel Canto aka flamelcanto

* Jin Kadaba aka Unknown


Para una lista actualizada de todo los colaboradores visite:
https://github.com/canaimagnulinux/canaimagnulinux.web.locales/contributors

.. _`sitio web del proyecto Canaima GNU/Linux`: http://canaima.softwarelibre.gob.ve/
.. _`localizaciones`: http://es.wikipedia.org/wiki/Internacionalización_y_localización
.. _`z3c.form`: https://pypi.python.org/pypi/z3c.form
.. _`plone.app.ldap`: https://pypi.python.org/pypi/plone.app.ldap
.. _`plone.app.caching`: https://pypi.python.org/pypi/plone.app.caching
.. _`plone.app.dexterity`: https://pypi.python.org/pypi/plone.app.dexterity
.. _`plone.schemaeditor`: https://pypi.python.org/pypi/plone.schemaeditor
.. _`plone.app.discussion`: https://pypi.python.org/pypi/plone.app.discussion
.. _`plone.app.caching`: https://pypi.python.org/pypi/plone.app.caching
.. _`quintagroup.analytics`: https://pypi.python.org/pypi/quintagroup.analytics
.. _`collective.nitf`: https://github.com/collective/collective.nitf
.. _`collective.geo.settings`: https://pypi.python.org/pypi/collective.geo.settings
.. _`pas.plugins.velruse`: https://pypi.python.org/pypi/pas.plugins.velruse
.. _`Products.PloneServicesCenter`: https://pypi.python.org/pypi/Products.PloneServicesCenter
.. _`Products.TinyMCE`: https://pypi.python.org/pypi/Products.TinyMCE
.. _`repositorio CanaimaGNULinux`: https://github.com/CanaimaGNULinux/canaimagnulinux.web.locales
.. _apertura de un ticket de soporte: https://github.com/CanaimaGNULinux/canaimagnulinux.web.locales/issues
